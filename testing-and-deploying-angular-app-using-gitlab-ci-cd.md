<!---
tags: angular, gitlab, continuous integration, continuous deployment
date: 2017-12-14T00:40:00+03:00
-->

Testing Building and Deploying Angular App Using GitLab CI/CD
=========

GitLab has many developer-friendly features and one of them is the Continuous Integration / Continuous Deployment system.
It means that you can run your both unit and end-to-end tests on GitLab server. It can be triggered by commits in branches, tags, or triggered manually.
First we need to understand the difference between continuous integration, continuous development and continuous deployment.

**Continuous integration**: Tests are triggered automatically when we commit, or open a pull request. Build and deployment are done manually.

**Continuous development**: Tests and builds are triggered automatically but deployment is manual.

**Contiuous deployment**: Tests are run, then project is built, if all success, deployment is done. Whole process is automatic.

GitLab allows us all of them above. It's upto you how much you want to automate this process.

**As I have done this for an Angular app, I will use configuration for Angular but you can easily change for your needs.**

# Angular e2e configuration for headless chrome

I want to run my e2e tests with headless chrome which is already installed in our docker image. So I add these lines to my `package.json`:

```json
...
"scripts": {
  ...
  "e2e:ci": "ng e2e --serve=true --aot --environment=prod --target=production --config protractor.headless.conf.js --webdriver-update=false",
  "pree2e:ci": "npm run webdriver-manager update -- --gecko=false",
  "webdriver-manager": "./node_modules/.bin/webdriver-manager"
}
...
```

Did you noticed that I use `protractor.headless.conf.js` instead of default `protractor.conf.js`? As we do not have GUI in our testing environment, we need to run headless chrome, so we have a new protractor configuration file. Just copy your default `protractor.conf.js` to `protractor.headless.conf.js` and change these lines:

```json
...
capabilities: {
  browserName: 'chrome',
  chromeOptions: {
    args: ["--headless", "--disable-gpu", "--window-size=800x600"],
  },
},
...
```

Thanks to [Carl Vuorinen](http://cvuorinen.net/2017/05/running-angular-tests-in-headless-chrome/) for his work. Now we are ready to configure some automation!

# The YAML file

GitLab CI/CD is configured by a YAML file named `.gitlab-ci.yml`. It should be located in your project root. GitLab opens a fresh docker machine for your needs. So you need a docker image which will suit your needs. I wrote e2e tests, so I use the image `trion/ng-cli-e2e`. So our file looks like this now:

```yaml
image: trion/ng-cli-e2e
```

GitLab will fetch the image and run our machine. Next, we define our stages. Let's add the following lines to our `.gitlab-ci.yml`:

```yaml
stages:
  - test_e2e
  - build
  - deploy
```

The stages define our steps. It means that `test_e2e` comes first. If it success, `build` is done. If it is successful too, then `deploy` is going to run. However GitLab still doesn't know anything about what to do in those stages. Let's define them!

```yaml
test_e2e:
  stage: test_e2e
  script:
    - npm install
    - npm run pree2e:ci
    - npm run e2e:ci
```

This is our `test_e2e` stage! The `script`s will be run in this step. If they are all successful, we can continue to next stage: `build`.

```yaml
build:
  stage: build
  script:
    - echo "Building"
    - npm install
    - ng build --target production --environment prod --aot --sourcemaps
```

Now we build the project. Notice that, we run `npm install` again. The reason is, each stage is a different process and runs in different docker container. But wait, we will need the `dist/` folder in our deployment stage! If everything is removed, what is the meaning of building our application? There is a solution for it: `artifacts`! Let's update our build stage:

```yaml
build:
  stage: build
  script:
    - echo "Building"
    - npm install
    - ng build --target production --environment prod --aot --sourcemaps
  artifacts:
    paths:
      - dist/
```

Now our `dist/` folder, which contains our built files, are saved as artifacts. We can continue to deployment:

```yaml
deploy_staging:
  stage: deploy
  script:
    - echo "Deploying to staging server"
    - tar -czvf dist.tar.gz dist
    - curl --fail -X POST -F deployment=@dist.tar.gz http://ege.sertcetin.com:3000/deploy
  environment:
    name: staging
  only:
    - master
  dependencies:
    - build
```

You might notice that there is an environment label now. This is the deployment for staging environment. You might have different deployments for your environments like production and staging. `only` label tells GitLab that this stage should be triggered only on commits to `master` branch. As we used `artifacts` in `build` stage, the `deploy` now depends on `build`. We define this in `dependencies` label.

When I was searching for a deployment strategy for my own server, I saw people did this by adding ssh keys and copying the dist folder by scp. I do not want to enter my ssh key to GitLab, so I made my own scripts. Using `tar` and `curl`, I package my dist folder as `dist.tar.gz` and send it to my server. No need for ssh, but some need for configuration in my server.

# Server side configuration

As we generally don't have any backend code for Angular projects, somehow we need to download and unzip our `dist.tar.gz`. We add some useful dependencies:

```bash
npm install --save-dev express multer
```

**express**: Pretty popular node.js framework.
**multer**: It is for handling file uploads.

Let's write those to `server/deploy.js`:

```js
// require our dependencies
const express = require('express');
const multer = require('multer');
const path = require('path');

const app = express();
const storage = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, './uploads');
  },
  filename: function(req, file, callback) {
    // save the file name using current date-time to avoid overwrites
    const filename = file.fieldname + '-' + Date.now() + '.tar' + path.extname(file.originalname);
    callback(null, filename);
  },
});
const upload = multer({
  storage: storage,
});
const { exec } = require('child_process');
const port = 3000; // our little backend program will use this port

// we define a route here: POST /deploy
app.post('/deploy', upload.single('deployment'), (request, response) => {
  const filePath = request.file.path;
  // we downloaded the file, now we untar it
  exec(`tar -xzvf ${filePath} -C .`, (err, stdout, stderr) => {
    // Notice that we send HTTP 200 if we success, and HTTP 500 if we fail. This is important for our curl to success or fail.
    if (!err) {
      response.status(200).send();
    } else {
      response.status(500).send();
    }
    console.log(err);
    console.log(stdout);
    console.log(stderr);
  });
});

// run the tiny app!
app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err);
  }

  console.log(`server is listening on ${port}`);
});
```

Now we have a small deployment app, working in our server. If you use nginx, you can use reverse proxy for `/deploy` subdirectory and don't open port 3000 to the universe. So, people who look for open ports in your server, won't notice that there is a secret deployment port.

# Conclusion and limits

That's all! Now when you push to your GitLab repository master branch, your continuous deployment system will start working. You can improve this by sending a notification to your customers or users telling that a new version is deployed. It feels pretty nice to automate some manual stuff, this is what we programmers do!

If you use GitLab CI/CD for open source projects, there are no limits. However for private repositories, each user and each group have 2000 minutes of using shared docker machines each month. So be careful to simplify your automation process.
